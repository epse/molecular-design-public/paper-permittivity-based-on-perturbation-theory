# Predicting the Relative Static Permittivity: a Group Contribution Method based on Perturbation Theory

In this repository, you can find the code for the paper ["Predicting the Relative Static Permittivity: a Group Contribution Method based on Perturbation Theory"](https://doi.org/10.1021/acs.jced.3c00323) by Lisa Rueben, Johannes Schilling, Philipp Rehner, Simon Müller, Timm Esper, André Bardow, and Joachim Gross published in *Journal of Chemical & Engineering Data*. 

The code allows calculating permittivities using the permittivity model developed in [this work](https://doi.org/10.1021/acs.jced.3c00323) by extending the model from [Neumaier et al. (2022)](https://doi.org/10.1016/j.fluid.2021.113346). The parameters for the permittivity model can either be taken from the parameter database `Parameters/PermittivityParameterDatabase.csv` or from the group contribution method.
Additionally, the code can calculate the densities required for computing the permittivities, either using the density correlation `Parameters/ParametersDensityCorrelation.csv` parametrized in this work, or PC-SAFT as equation of state. The permittivity parameters in `Parameters/PermittivityParameterDatabase.csv` were fitted using the densities from the density correlation. Therefore, the density correlation should be used when using these parameters. 
If the user wishes to use PC-SAFT instead, the permittivity parameters should taken from `Parameters/PermittivityParameterDatabasePCSAFT.csv`.

## Installation

The provided code has been developed and tested on Linux, but should also run on Windows/MacOS. You only need a running Python version with the most important packages (such as `pandas`, `numpy`, `ipython`, and `matplotlib`) installed. If you wish to use PC-SAFT, you need to install the [`feos`](https://feos-org.github.io/feos/index.html) package (see below), if you want to carry out fragmentations yourself, you need the [`rdkit`](https://www.rdkit.org/) package.

Generally, you can use our `environment.yml` to set up your Python environment. Using anaconda, you can create your environment from the file as follows: Go to the location of the file and execute:
```
conda env create -f environment.yml
```
If you want to use your own environment, here is how to install [`feos`](https://feos-org.github.io/feos/index.html):

The Python package [`feos`](https://feos-org.github.io/feos/index.html) computes thermodynamic properties with the PC-SAFT equation of state. This installation is a simple `pip` installation (more information in the [`feos` documentation](https://feos-org.github.io/feos/index.html)). We use `feos 0.4.1`:

```
pip install feos==0.4.1
```
If you use your own enviroment, you might run into additional packages you need to install.

## Calculating permittivities

You can learn how to calculate permittivities using the tutorial file `Tutorial_Calculate_Permittivities.ipynb`. Here, you find a detailed explanation of all the different ways to create permittivity parameters.

Here, you find a list of the files included in this Git and their meaning:

- `Molecule/`: this folder contains the algorithms needed to fragment a molecule from its SMILES code, and the fragmentation used in this work.
- `Parameters/`: this folder contains all parameters for density correlations, permittivity model, ... used in this work.
- `Permittivity.py`: contains permittivity model and permittivity parameters.
- `Thermo.py`: computes densities and contains all thermodynamic models used in this work.
- `environment.yml`: yml-file to create conda environment.

## Further figures
The permittivity-temperature diagrams are available in a separate [Zenodo repository](https://zenodo.org/record/8252886), due to repository size restrictions on Gitlab.