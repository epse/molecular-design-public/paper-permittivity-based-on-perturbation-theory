import numpy as np
import scipy.constants as constants

from feos.pcsaft import PcSaftParameters, ChemicalRecord, SegmentRecord, Identifier, IdentifierOption
from feos.eos import EquationOfState, State, PhaseEquilibrium
from feos.si import KELVIN, PASCAL, MOL, DECI, METER


class DensityCalculator:

    """
    Object to calculate density with. Contains either DensityCorrelation or EoS,
    depending on density_option.

    """

    def __init__(self, density_correlation=None, eos=None):

        """
        Initialize DensityCalculator

        Parameters
        ----------
            density_correlation: DensityCorrelation, optional
            DensityCorrelation object.

            eos: EoS, optional
            Equation of state object.

        """

        self.density_correlation = density_correlation
        self.eos = eos


    def setup_density(self, substance_id, density_option, cas=None, density_data=None, fragmentation=None, molecule=None, feasible_groups=None, t_min=None):

        """ Set up density functionalities depending on density option.

        Parameters
        ----------
            substance_id: int
            DDB number of substance

            density_option: str
            Either "correlation", "pc-saft", or "gc-pc-saft"

            cas: str, optional
            CAS of substance (required for GC-PC-SAFT and PC-SAFT)

            density_data: pd.DataFrame(), optional
            Dataframe with info about density correlation coefficients
            (required for "correlation")

            fragmentation: Fragmentation, optional
            Fragmentation class (required only for GC-PC-SAFT and PC-SAFT)

            molecule: dict
            Dictionary with groups as keys and frequency as values.

            feasible_groups: list, optional
            List of feasible groups (required only for GC-PC-SAFT)

            t_min: f64, optional
            Minimum temperature (required only for GC-PC-SAFT and PC-SAFT)

        Returns
        ----------
            status: int
            Status of density 

            error message: str
            Potential error message

            density: DensityCorrelation or EoS
            Object where density is calculated with

        """

        ### Create density objects

        if density_option == "correlation":

            if density_data is None:
                raise Exception ("Provide density_data for setting up density with correlation.")
            # Create DensityCorrelation
            if substance_id not in density_data.index:
                print('No density data for this molecule. Skipping!',
                    substance_id)
                return -1, "no_density_data", None
            else:
                density = DensityCorrelation(density_data, substance_id)   

        elif density_option == "gc-pc-saft":

            if fragmentation is None and molecule is None:
                raise Exception ("Provide fragmentation or molecule for setting up density with gc-pc-saft.")
            if t_min is None:
                raise Exception ("Provide t_min for setting up density with gc-pc-saft.")
            # Create EoS from fragmentation
            # Get fragmentation if not already given:
            if fragmentation is not None:
                molecule, success, _, n_functional_groups = fragmentation.get_fragmentation(substance_id)

                if success == 0:
                    print("Fragmentation not successful. Skipping!")
                    return -1, "nonsuccessful_fragmentation", None

                # Check if fragmentation is feasible with given groups and groups to fit
                if feasible_groups is not None:
                    for f in molecule.keys():
                        if f not in feasible_groups:
                            print("Fragmentation not feasible for the given groups. Skipping!")
                            return -1, "infeasible_fragmentation", None

                # Sauer restrictions: max. 1 polar or associating functional group.
                if n_functional_groups > 1:
                    print("More than 1 polar/associating group. Skipping!")
                    return -1, "multifunctional", None

            # Create EoS
            density = EoS([molecule], option="gc-pc-saft") 
            density.t_max = State.critical_point(density.eos).temperature / KELVIN   
            
            
        elif density_option == "pc-saft":

            if cas is None or t_min is None:
                raise Exception ("Provide cas and t_min for setting up density with pc-saft.")

            # Try to find PC-SAFT parameters in .json file using the CAS as search option
            try:
                density = EoS(option="pc-saft", cas=[cas])
                # Find out real critical temperature   
                density.t_max = State.critical_point(density.eos).temperature / KELVIN   
            except:
                print('No PC-SAFT parameters found for molecule. Skipping!',
                        substance_id)
                return -1, "no_saft_parameters", None
        else:
            raise Exception("Density option unknown.")

        # get minimum temperature 
        if t_min is not None:
            density.t_min = t_min
        else:
            density.t_min = 0
            
        # Set correct density object as return value 
        if density_option == "correlation":
            self.density_correlation = density
            self.n_comp = 1
            return 0, "", self.density_correlation
        else:
            self.eos = density
            self.n_comp = 1
            return 0, "", self.eos
        
    def setup_mixture_density(self, mixture_id, cas):

        """ Set up density functionalities for mixture. Only density option possible 

        Parameters
        ----------
            mixture_id: str
            Mixture id, e.g., "229;72"

            cas: list(str)
            CAS of all substances in mixture 

        Returns
        ----------
            status: int
            Status of density 

            error message: str
            Potential error message

            density: EoS
            Object where density is calculated with

        """

        # Try to find PC-SAFT parameters in .json file using the CAS as search option
        try:
            density = EoS(option="pc-saft", cas=cas)  
        except:
            print('No PC-SAFT parameters found for all mixture components. Skipping!',
                    mixture_id)
            return -1, "no_saft_parameters", None
       
        self.eos = density
        self.n_comp = len(cas)
        
        return 0, "", self.eos


    def calculate_density_raster(self, t_min, unit="#/A3"):
        
        """ Calculates a density raster for a given minimum temperature. Only for pure component.
        Maximum temperature is derived from max. temperature of density calculator.

        Parameters
        ----------
            t_min: f64
            Minimum temperature for temperature raster (in Kelvin)

        Returns
        ----------
            temperature_raster: Numpy-Array (f64)
            Array with temperatures.

            density_raster: Numpy-Array (f64)
            Array with respective densities.

        """
        # Check: only for pure substance
        if self.n_comp > 1:
            raise Exception("Pure density raster only available for pure substance.")
        
        # Either use density correlation to calculate density
        if self.density_correlation is not None:
            temperature_raster = np.linspace(t_min, 0.999*self.density_correlation.t_max, 200)
            density_raster = self.density_correlation.evaluate(temperature_raster, unit=unit)
        # or use Equation of State to calculate density
        elif self.eos is not None:
            # Get critical temperature as max. temperature
            cp = State.critical_point(self.eos.eos)
            t_c = cp.temperature / KELVIN
            p_c = cp.pressure() / PASCAL
            # Create temperature raster
            temperature_raster = np.linspace(t_min, t_c, 100)
            # Create pressure raster that fits to temperature raster
            pressure_raster = []
            # Pressure as max. of saturation pressure and 1 bar (below critical point)
            for t in temperature_raster[0:-1]:
                psat = PhaseEquilibrium.pure(self.eos.eos, temperature_or_pressure=t*KELVIN).liquid.pressure() / PASCAL
                pressure_raster.append(max(psat, 1e5))
            pressure_raster.append(p_c)
            pressure_raster = np.array(pressure_raster)
            # Create density raster
            density_raster = self.eos.density(temperature_raster, pressure_raster, unit=unit)

        return temperature_raster, density_raster



class DensityCorrelation:

    """
    Class DensityCorrelation is the object to calculate the density with 
    if the liquid density correlation should be used.
    
    """

    def __init__(self, density_data, substance_id):
        
        """ Initialize density correlation.

        Parameters
        ----------
            density_data: pd.DataFrame(), optional
            Dataframe with info about density correlation coefficients
            (required for "correlation")

            substance_id: int
            DDB number of substance

        """

        # Fill correlation object with data, coefficients, maximal and minimal temperatures
        data = density_data.loc[substance_id]

        self.rho_crit = data["rho_crit/(mol/dm3)"]
        self.A = data["A"]
        self.T_crit = data["T_crit/K"] 
        self.B = data["B"]
        self.t_max = data["T_max/K"]
        self.t_min = 0

    def evaluate(self, t, unit="mol/dm3"):

        """ Evaluate density for given temperature(s).

        Parameters
        ----------
            t: Numpy-Array (f64)
            Temperatures (in Kelvin) for which the density should be evaluated.

            unit: str, optional
            Target unit of density. Default: mol/dm3, also possible: #/A3

        """

        rho_crit = self.rho_crit
        A = self.A
        T_crit = self.T_crit
        B = self.B

        rho = rho_crit * A**(-(1-(t/T_crit))**B)

        if unit == "#/A3":
            rho = constants.Avogadro * 1e-27 * rho

        return rho


class EoS:

    """
    Class EoS is the object to calculate the density with 
    if PC-SAFT as equation of state should be used.
    
    """

    def __init__(self, molecules=None, cas=["00-00-00"], option="pc-saft", filename_segment_parameters="Parameters/sauer2014_homo.json", filename_pure_parameters="Parameters/Esper2023_PC-SAFT_pure_parameters.json"):

        """ Initialize thermodynamic model for molecule.

        Parameters
        ----------
            molecule: List(dict)
            Dictionary with molecular structure of components: e.g., molecule = {"CH3": 2, "CH2": 1}

            cas: List(str), optional
            CAS number of components

            option: str, optional
            Either "pc-saft" or "gc-pc-saft", Default: "pc-saft"

            filename_segment_parameters: str, optional
            Directory to json-file with homo GC parameters.

            filename_pure_parameters: str, optional
            Directory to json-file with pure parameters.
        
        """
        
        if option == "gc-pc-saft":
            # Create PC-SAFT parameters and Equation of State

            chemical_records = []
            # For each molecule:
            for mol in molecules:

                # Identifier
                id = Identifier(cas=str(cas))

                # ChemicalRecord & SegmentRecord
                segments = []
                for k, v in mol.items():
                    temp = [k for i in range(int(v))]
                    segments += temp

                chemical_records.append(ChemicalRecord(identifier=id, segments=segments))
            
            # Segment records: information about segment parameters
            sr = SegmentRecord.from_json(filename_segment_parameters)

            # Parameters from ChemicalRecords & SegmentRecord
            self.param = PcSaftParameters.from_segments(
                chemical_records=chemical_records, segment_records=sr)
            
            # Create pure EoS
            self.pure_eos = []
            for cr in chemical_records:
                param_temp = PcSaftParameters.from_segments(
                chemical_records=[cr], segment_records=sr)
                self.pure_eos.append(EquationOfState.pcsaft(param_temp))

        elif option == "pc-saft":
            try:
                self.param = PcSaftParameters.from_json(substances=cas, pure_path=filename_pure_parameters, search_option=IdentifierOption.Cas)
            except:
                raise Exception("CAS number not found.")
            
            # Create pure EoS
            self.pure_eos = []
            for c in cas:
                param_temp = PcSaftParameters.from_json(substances=[c], pure_path=filename_pure_parameters, search_option=IdentifierOption.Cas)
                self.pure_eos.append(EquationOfState.pcsaft(param_temp))

        # EquationOfState
        self.eos = EquationOfState.pcsaft(self.param)
        self.n_comp = len(self.param.pure_records)


    def density(self, temperature, pressure, molefraction=None, unit="mol/dm3"):

        """ Calculate density for a set of temperatures and pressures.

        Parameters
        ----------
            temperature: Numpy-Array (f64)
            Temperature in K

            pressure: Numpy-Array (f64)
            Pressure in Pascal

            molefraction: list(Numpy-Array (f64)), optional
            Molefraction of all components

            unit: str, optional
            Target unit of density. Default: mol/dm3, also possible: #/A3

        Returns
        ----------
            density: Numpy-Array (f64)
            Density in desired unit

        """

        # Check if molefraction is provided for mixture
        if self.n_comp > 1 and molefraction is None:
            raise Exception ("Provide molefraction for mixture.")

        # Prepare density
        density = []

        # FOR PURE COMPONENT
        if self.n_comp == 1:
            # Iterate over temperature and pressure
            for t, p in zip(temperature, pressure):
                
                # Determine pressure for nan entries: maximum of 1 bar and saturation pressure at temperature t
                if np.isnan(p):
                    psat = PhaseEquilibrium.pure(self.eos, temperature_or_pressure=t*KELVIN).liquid.pressure() / PASCAL
                    p = max(psat, 1e5) * PASCAL
                else:
                    p = p * PASCAL


                # Create liquid state
                state = State(self.eos, temperature=t*KELVIN,
                            pressure=p, density_initialization="liquid")

                # Evaluate density
                d = state.density / (MOL/(DECI*METER)**3)

                # Convert to particles per cubic Angström 
                if unit == "#/A3":
                    d = constants.Avogadro * 1e-27 * d
                elif unit == "mol/dm3":
                    pass
                else: 
                    raise Exception("Desired unit for density not available, choose either mol/dm3 or #/A3.")

                density.append(d)
        
        # FOR MIXTURE
        else:
            # Check if molefraction is provided for mixture
            if self.n_comp > 2 and self.n_comp != len(molefraction):
                raise Exception ("Provide molefractions for each mixture component.")

            # Iterate over temperature
            for i in range(len(temperature)): 

                # Find out pressure and set to 1e5 Pa if not given
                if np.isnan(pressure[i]):
                    pressure[i] = 1e5  # in Pa

                # Collect molefractions for (T_i, p_i)
                x = [molefraction[j][i] for j in range(self.n_comp)]

                # Create state 
                state = State(self.eos, temperature=temperature[i]*KELVIN,
                                    pressure=pressure[i]*PASCAL, density_initialization="liquid", moles=np.array(x)* MOL)
                
                # Calcaulate density in mol/dm^3
                d = state.density / (MOL/(DECI*METER)**3)

                # Convert to particles per cubic Angström 
                if unit == "#/A3":
                    d = constants.Avogadro * 1e-27 * d
                elif unit == "mol/dm3":
                    pass
                else: 
                    raise Exception("Desired unit for density not available, choose either mol/dm3 or #/A3.")
                
                density.append(d)

        return np.array(density)
