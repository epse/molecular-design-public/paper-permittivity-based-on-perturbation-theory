from rdkit import Chem
import copy

functional_groups = [# POLAR GROUPS
    # aldehydes -CH=O : neighbor has to be C
    "CHdO",
    # ketones >C=O : both neighbors have to be C, no rings
    ">CdO",
    # ethers -O- : both neighbors have to be C, no rings, no formates/esters
    "O",
    # formates -OCH=O, neighbor has to be C
    "HCOO",
    # esters -OC(=O)- : both neighbors have to be C, no rings
    "COO",
    "OCH2",
    "OCH3",
    #
    # ALCOHOLS
    # primary alcohols -OH : neighbor has to be CH2
    "OH_1",
    # secondary alcohols -OH : neighbor has to be >CH
    "OH_2",
    # tertiary alcohols -OH : neighbor has to be >C<
    "OH_3",
    #
    # AMINES
    # primary amines -NH2 : neighbor has to be C, no methylamine
    "NH2"
    ]

smarts = {
    # NON-POLAR, NON-ASSOCIATING ALIPHATIC FUNCTIONAL GROUPS
    # alkanes -CH3 : no ethane, no methanol, no methylamine
    "CH3": "[CH3;!$([CH3][CH3]);!$([CH3][OH]);!$([CH3][NH2])]",
    # alkanes -CH2- : no ring
    "CH2": "[CX4H2;!R]",
    # alkanes >CH- : no ring
    ">CH": "[CX4H1;!R]",
    # alkanes >C< : no ring
    ">C<": "[CX4H0;!R]",
    # alkenes =CH2 : no ring, no ethylene, no formaldehyde
    "dCH2": "[CX3H2;!R;!$([CH2]=[CH2]);!$([CH2]=O)]",
    # alkenes =CH- : no ring, no aldehyde
    "dCH": "[CX3H1;!R;!$([CX3H1]=O)]",
    # alkenes =C< : no ring, no ketone
    "dC<": "[CX3H0;!R;!$([CX3H0]=O)]",
    # alkynes ≡C- : no ring
    "tC": "[$([CH0]#C);!R]",
    # alkynes ≡CH : no ethyne
    "tCH": "[$([CH1]#C);!$([CH1]#[CH1])]",
    #
    # AROMATIC AND CYCLIC GROUPS
    # aromats =CH- : ring
    "CH_arom": "[cH1;R]",
    # aromats =C< : ring
    "C_arom": "[cH0;R]",
    # cyclopentanes -CH2- : ring of size 5
    "CH2_pent": "[CX4H2;r5]",
    # cyclopentanes >CH- : ring of size 5
    "CH_pent": "[CX4H1;r5]",
    # cyclohexanes -CH2- : ring of size 6
    "CH2_hex": "[CX4H2;r6]",
    # cyclohexanes >CH- : ring of size 6
    "CH_hex": "[CX4H1;r6]",
    #
    # POLAR GROUPS
    # aldehydes -CH=O : neighbor has to be C
    "CHdO": "[$([CH1][C,c])]=O",
    # ketones >C=O : both neighbors have to be C, no rings
    ">CdO": "[$([C]([C,c])([C,c]));!R]=O",
    # ethers -O- : both neighbors have to be C, no rings, no formates/esters
    "O": "[$([O]([C,c])[C,c]);!R;!$([O][C,c]=O)]",
    # formates -OCH=O, neighbor has to be C
    "HCOO": "[CH1](=O)[$([OH0][C,c])]",
    # esters -OC(=O)- : both neighbors have to be C, no rings
    "COO": "[$([CH0][C,c]);!R](=O)[$([OH0]([C,c])[C,c])]",

    #
    # ALCOHOLS
    # primary alcohols -OH : neighbor has to be CH2
    "OH_1": "[$([OH][CX4H2])]",
    # secondary alcohols -OH : neighbor has to be >CH
    "OH_2": "[$([OH][CX4H1])]",
    # tertiary alcohols -OH : neighbor has to be >C<
    "OH_3": "[$([OH][CX4H0])]",
    #
    # AMINES
    # primary amines -NH2 : neighbor has to be C, no methylamine
    "NH2": "[$([NH2][C,c]);!$([NH2][CH3])]",
    # secondary amines >NH : both neighbors have to be C, no rings
    "NH": "[$([NH1]([C,c])[C,c])]",
    # tertiary amines >N : all neighbors have to be C, no rings
    ">N": "[$([NH0]([C,c])([C,c])[C,c])]",
    #
    # HALOGENS
    # fluorinated components F : neighbor has to be C
    # "F": "[$(F[C])]",
    # fluorinated components F : neighbor has to be C, no other halogens on the C
    "F_1": "[$(F[C,c]);!$(F[C,c][F,Cl])]",
    # fluorinated components F : neighbor has to be C, one other halogen on the C
    "F_2": "[$(F[C,c][F,Cl]);!$(F[C,c]([F,Cl])[F,Cl])]",
    # fluorinated components F : neighbor has to be C, two other halogens on the C
    "F_3": "[$(F[C,c]([F,Cl])[F,Cl]);!$(F[C,c]([F,Cl])([F,Cl])[F,Cl])]",
    # fluorinated components F : neighbor has to be C, three other halogens on the C
    "F_4": "[$(F[C,c]([F,Cl])([F,Cl])[F,Cl])]",
    # chlorinated components Cl : neighbor has to be C
    # "Cl": "[$(Cl[C])]",
    # cholrinated components Cl : neighbor has to be C, no other halogens on the C
    "Cl_1": "[$(Cl[C,c]);!$(Cl[C,c][F,Cl])]",
    # cholrinated components Cl : neighbor has to be C, one other halogen on the C
    "Cl_2": "[$(Cl[C,c][F,Cl]);!$(Cl[C,c]([F,Cl])[F,Cl])]",
    # cholrinated components Cl : neighbor has to be C, two other halogens on the C
    "Cl_3": "[$(Cl[C,c]([F,Cl])[F,Cl]);!$(Cl[C,c]([F,Cl])([F,Cl])[F,Cl])]",
    # cholrinated components Cl : neighbor has to be C, three other halogens on the C
    "Cl_4": "[$(Cl[C,c]([F,Cl])([F,Cl])[F,Cl])]",
    #
    # SINGLE MOLECULES
    # methane
    "CH4": "[CH4]",
    # ethane
    "C2H6": "[CH3][CH3]",
    # ethylene
    "C2H4": "[CH2]=[CH2]",
    # ethyne
    "C2H2": "[CH1]#[CH1]",
    # formaldehyde
    "CH2dO": "[CH2]=O",
    # methanol
    "CH3OH": "[CH3][OH]",
    # methylamine
    "CH3NH2": "[CH3][NH2]",
    # carbon dioxide
    "CO2": "O=C=O",
    # water
    "H2O": "[OH2]",
}

fragments_sauer = [
    "CH3",
    "CH2",
    ">CH",
    ">C<",
    "dCH2",
    "dCH",
    "dC<",
    "CtCH",
    "CH_arom",
    "C_arom",
    "CH2_pent",
    "CH_pent",
    "CH2_hex",
    "CH_hex",
    "CHdO",
    ">CdO",
    "OCH2",
    "OCH3",
    "HCOO",
    "COO",
    "OH_1",
    "NH2",
]


smarts_nC = {
    # NON-POLAR, NON-ASSOCIATING ALIPHATIC FUNCTIONAL GROUPS
    # alkanes -CH3 : no ring, no ethane, no methanol, no methylamine
    "CH3": 1,
    # alkanes -CH2- : no ring
    "CH2": 1,
    # alkanes >CH- : no ring
    ">CH": 1,
    # alkanes >C< : no ring
    ">C<": 1,
    # alkenes =CH2 : no ring, no ethylene, no formaldehyde
    "dCH2": 1,
    # alkenes =CH- : no ring, no aldehyde
    "dCH": 1,
    # alkenes =C< : no ring, no ketone
    "dC<": 1,
    # alkynes, -C≡CH
    "CtCH": 2,
    # alkynes ≡C- : no ring
    "tC": 1,
    # alkynes ≡CH : no ethyne
    "tCH": 1,
    #
    # AROMATIC AND CYCLIC GROUPS
    # aromats =CH- : ring
    "CH_arom": 1,
    # aromats =C< : ring
    "C_arom": 1,
    # cyclopentanes -CH2- : ring of size 5
    "CH2_pent": 1,
    # cyclopentanes >CH- : ring of size 5
    "CH_pent": 1,
    # cyclohexanes -CH2- : ring of size 6
    "CH2_hex": 1,
    # cyclohexanes >CH- : ring of size 6
    "CH_hex": 1,
    #
    # POLAR GROUPS
    # aldehydes -CH=O : neighbor has to be C
    "CHdO": 1,
    # ketones >C=O : both neighbors have to be C, no rings
    ">CdO": 1,
    # ethers -O- : both neighbors have to be C, no rings, no formates/esters
    "O": 0,
    # formates -OCH=O, neighbor has to be C
    "HCOO": 1,
    # esters -OC(=O)- : both neighbors have to be C, no rings
    "COO": 1,
    "OCH2": 1,
    "OCH3": 1,
    #
    # ALCOHOLS
    # primary alcohols -OH : neighbor has to be CH2
    "OH_1": 0,
    # secondary alcohols -OH : neighbor has to be >CH
    "OH_2": 0,
    # tertiary alcohols -OH : neighbor has to be >C<
    "OH_3": 0,
    #
    # AMINES
    # primary amines -NH2 : neighbor has to be C, no methylamine
    "NH2": 0,
    # secondary amines >NH : both neighbors have to be C, no rings
    "NH": 0,
    # tertiary amines >N : all neighbors have to be C, no rings
    ">N": 0,
    #
    # HALOGENS
    # fluorinated components F : neighbor has to be C
    # "F": "[$(F[C])]",
    # fluorinated components F : neighbor has to be C, no other halogens on the C
    "F_1": 0,
    # fluorinated components F : neighbor has to be C, one other halogen on the C
    "F_2": 0,
    # fluorinated components F : neighbor has to be C, two other halogens on the C
    "F_3": 0,
    # fluorinated components F : neighbor has to be C, three other halogens on the C
    "F_4": 0,
    # chlorinated components Cl : neighbor has to be C
    # "Cl": "[$(Cl[C])]",
    # cholrinated components Cl : neighbor has to be C, no other halogens on the C
    "Cl_1": 0,
    # cholrinated components Cl : neighbor has to be C, one other halogen on the C
    "Cl_2": 0,
    # cholrinated components Cl : neighbor has to be C, two other halogens on the C
    "Cl_3": 0,
    # cholrinated components Cl : neighbor has to be C, three other halogens on the C
    "Cl_4": 0,
    #
    # SINGLE MOLECULES
    # methane
    "CH4": 1,
    # ethane
    "C2H6": 2,
    # ethylene
    "C2H4": 2,
    # formaldehyde
    "CH2dO": 1,
    # methanol
    "CH3OH": 1,
    # methylamine
    "CH3NH2": 1,
    # carbon dioxide
    "CO2": 1,
    # water
    "H2O": 0,
}

def get_smarts(sauer=1):

    temp = copy.deepcopy(smarts)

    if sauer: 
        all_segments = [i for i in smarts.keys()]
        for seg in all_segments:
            if seg not in fragments_sauer:
                del temp[seg]

    return smarts

def get_smarts_nC():
    return smarts_nC

def get_functional_groups():
    return functional_groups

def get_fragments_sauer():
    return fragments_sauer

def fragment_molecule(smiles):
    mol = Chem.MolFromSmiles(smiles)
    atoms = mol.GetNumHeavyAtoms()

    # find the location of all fragment using the given smarts
    matches = dict(
        (g, mol.GetSubstructMatches(Chem.MolFromSmarts(smarts[g]))) for g in smarts
    )
    bonds = [(b.GetBeginAtomIdx(), b.GetEndAtomIdx()) for b in mol.GetBonds()]
    #print(dict((s,x) for s,x in matches.items() if len(x) > 0))
    return convert_matches(atoms, matches, bonds)


def convert_matches(atoms, matches, bonds):
    # check if every atom is captured by exatly one fragment
    identified_atoms = [i for l in matches.values() for k in l for i in k]
    unique_atoms = set(identified_atoms)
    if len(unique_atoms) == len(identified_atoms) and len(unique_atoms) == atoms:
        # Translate the atom indices to segment indices (some segments contain more than one atom)
        segment_indices = sorted(
            (sorted(k), group) for group, l in matches.items() for k in l
        )
        segments = [group for _, group in segment_indices]
        segment_map = [i for i, (k, _) in enumerate(segment_indices) for j in k]
        bonds = [(segment_map[a], segment_map[b]) for a, b in bonds]
        bonds = [(a, b) for a, b in bonds if a != b]
        return segments, bonds
    raise Exception("Molecule cannot be fragmented with the given SMARTS!")


def fragment_to_sauer_groups(smiles):
    segments, bonds = fragment_molecule(smiles)
    mol = Chem.MolFromSmiles(smiles)
    Chem.GetSymmSSSR(mol)
    rings = mol.GetRingInfo().NumRings()
    
    if rings > 1:
        raise Exception("Only molecules with up to 1 ring are allowed!")
        
    segments, bonds = convert_ether_groups(segments, bonds)
    segments, bonds = convert_alkyne_groups(segments, bonds)
    return segments, bonds


def convert_ether_groups(segments, bonds):
    ethers = sum(1 for group in segments if group == "O")
    if ethers == 0:
        return segments, bonds
    elif ethers == 1:
        index = sum(i for i, group in enumerate(segments) if group == "O")
        neighbors = []
        for a, b in bonds:
            if a == index:
                right = (b, segments[b])
            elif b == index:
                left = (a, segments[a])

        if left[1] == "CH3":
            index2 = left[0]
            new_group = "OCH3"
        elif right[1] == "CH3":
            index2 = right[0]
            new_group = "OCH3"
        elif left[1] == "CH2":
            index2 = left[0]
            new_group = "OCH2"
        elif right[1] == "CH2":
            index2 = right[0]
            new_group = "OCH2"
        else:
            raise Exception(
                "The ether is not compatible with the groups by Sauer et al.!"
            )
        matches = {new_group: ((index, index2),)}
        for i, group in enumerate(segments):
            if i in {index, index2}:
                continue
            if group in matches:
                matches[group] = matches[group] + ((i,),)
            else:
                matches[group] = ((i,),)
        return convert_matches(len(segments), matches, bonds)
    else:
        raise Exception(
            "The conversion is only implemented for molecules with one ether group!"
        )


def convert_alkyne_groups(segments, bonds):
    matches = {'CtCH': ()}
    for i,group in enumerate(segments):
        if group not in {"tC", "tCH"}:
            if group in matches:
                matches[group] += ((i,),)
            else:
                matches[group] = ((i,),)
    for i,j in bonds:
        if segments[i] == "tC" and segments[j] == "tC":
            raise Exception(
                "Only terminal alkynyl groups are allowed!"
            )
        elif segments[i] == "tC" and segments[j] == "tCH":
            matches['CtCH'] += ((i,j),)
        elif segments[i] == "tCH" and segments[j] == "tC":
            matches['CtCH'] += ((j,i),)
    return convert_matches(len(segments), matches, bonds)
