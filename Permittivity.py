import scipy.constants as constants
import math
import numpy as np
import pandas as pd

class PermittivityModel:

    def __init__(self, permittivity_parameters, options=None):

        self.param = permittivity_parameters
        self.options = options

    def evaluate_permittivity(self, temperature, density):

        """ Evaluate relative static permittivity according to Neumaier et al. (2022)

        Parameters
        ----------
            temperature: Numpy-Array (f64)
            Temperature

            density: Numpy-Array (f64)
            Density in #/A3

        Returns
        ----------
            epsilon_r: Numpy-Array (f64)
            Permittivity (model result)

        """

        beta = 1 / (constants.k * temperature)

        y_mu2 = 4/9 * math.pi * beta * density

        y = y_mu2 * (self.param.a11_mu2 * 1e-19 + 3 / beta * self.param.a12_alpha)

        I_rho = 1 + self.param.a2 * (np.exp(-y) - 1)

        # Calculate epsilon
        epsilon_r = 1 + 3 * y * (1 + y + (17/16*I_rho - 1) * y**2)

        return epsilon_r
    

    def evaluate_mixture_permittivity(self, temperature, mixture_density, molefraction, psi_ij=None):

        """ Evaluate relative static permittivity of a mixture according to Neumaier et al. (2022)

        Parameters
        ----------
            temperature: Numpy-Array (f64)
            Temperature.

            mixture_density: Numpy-Array (f64)
            Density in #/A3.

            molefraction: List(Numpy-Array (f64)) with n entries
            Molefraction of each component.

            psi_ij: Numpy-Array (f64) n x n, optional, default: None
            Binary interaction matrix.

        Returns
        ----------
            epsilon_r: Numpy-Array (f64)
            Permittivity (model result)

        """
        # Number of mixture components
        n_comp = len(self.param.a11_mu2)

        if len(molefraction) != n_comp:
            raise Exception ("Number of components for which mole fractions are provided does not fit mixture components.")

        beta = 1 / (constants.k * temperature)

        if psi_ij is None:
            y = 4/9 * math.pi * beta * mixture_density * sum(molefraction[i] * self.param.a11_mu2[i] * 1e-19 + 3 * molefraction[i] / beta * self.param.a12_alpha[i] for i in range(n_comp))
        else:
            if psi_ij.shape[0] == psi_ij.shape[1] == n_comp:
                y = 4/9 * math.pi * beta * mixture_density * sum(sum( molefraction[i] * molefraction[j] * 0.5 * ((self.param.a11_mu2[i] + self.param.a11_mu2[j]) * 1e-19 + 3 / beta * (self.param.a12_alpha[i] + self.param.a12_alpha[j])) * (1-psi_ij[i,j]) for j in range(n_comp)) for i in range(n_comp))
            else:
                raise Exception ("Dimension of psi_ij does not fit mixture components.")

        I_rho = 1 + sum(self.param.a2[i] * molefraction[i] for i in range(n_comp)) * (np.exp(-y) - 1)

        # Calculate epsilon
        epsilon_r = 1 + 3 * y * (1 + y + (17/16*I_rho - 1) * y**2)

        return epsilon_r


class PermittivityError:

    def __init__(self, epsilon_r_calc, epsilon_r_exp):

        self.point_mard = self.calculate_point_mard(epsilon_r_calc, epsilon_r_exp)
        self.point_mad = self.calculate_point_mad(epsilon_r_calc, epsilon_r_exp)
        self.mard = np.mean(self.point_mard)
        self.rmse = self.calculate_rmse(epsilon_r_calc, epsilon_r_exp)
        self.mad = self.calculate_mad(epsilon_r_calc, epsilon_r_exp)


    def __str__(self):

        return "MARD: " + str(np.round(self.mard,1)) + "%, RMSE: " + str(np.round(self.rmse,2)) + " MAD: " + str(np.round(self.mad,2))


    def calculate_point_mard(self, epsilon_r_calc, epsilon_r_exp):

        """ Calculate mean absolute relative deviation for each point.

            Parameters
            ----------
                epsilon_r_calc: Numpy-Array (f64)
                Calculated dielectric constant.
                
                epsilon_r_exp: Numpy-Array (f64)
                Experimental dielectric constant.
            
            Returns
            ----------
                mard: Numpy-Array (f64)
                Mean absolute relative deviation for each point.

        """

        return abs((epsilon_r_exp-epsilon_r_calc)/epsilon_r_exp) * 100
    
    def calculate_point_mad(self, epsilon_r_calc, epsilon_r_exp):

        """ Calculate mean absolute deviation for each point.

            Parameters
            ----------
                epsilon_r_calc: Numpy-Array (f64)
                Calculated dielectric constant.
                
                epsilon_r_exp: Numpy-Array (f64)
                Experimental dielectric constant.
            
            Returns
            ----------
                mad: Numpy-Array (f64)
                Mean absolute deviation for each point.

        """

        return abs(epsilon_r_exp-epsilon_r_calc)
    
    def calculate_rmse(self, epsilon_r_calc, epsilon_r_exp):

        """ Calculate root mean square error for each point.

            Parameters
            ----------
                epsilon_r_calc: Numpy-Array (f64)
                Calculated dielectric constant.
                
                epsilon_r_exp: Numpy-Array (f64)
                Experimental dielectric constant.
            
            Returns
            ----------
                rmse: f64
                Root mean square error for substance.

        """
        
        return (np.sum((epsilon_r_exp-epsilon_r_calc)**2) / len(epsilon_r_calc))**(0.5)
        
    def calculate_mad(self, epsilon_r_calc, epsilon_r_exp):

        """ Calculate mean absolute deviation for each point.

            Parameters
            ----------
                epsilon_r_calc: Numpy-Array (f64)
                Calculated dielectric constant.
                
                epsilon_r_exp: Numpy-Array (f64)
                Experimental dielectric constant.
            
            Returns
            ----------
                mad: f64
                Mean absolute deviation for substance.

        """

        return (np.sum(abs(epsilon_r_exp-epsilon_r_calc)) / len(epsilon_r_calc))


class PermittivityParameters:

    def __init__(self, molecule=None, a11_mu2=None, a12_alpha=None, a2=None, query=None, filename_database="Parameters/PermittivityParameterDatabase.csv", filename_gc_database="Parameters/GCPermittivityParameters.csv", query_option="substance_id"):

        self.status = -1

        # First option: create directly from parameter values for the three parameters, for mixtures from lists
        if a11_mu2 is not None or a12_alpha is not None:
            self.from_parameter_values(a11_mu2=a11_mu2, a12_alpha=a12_alpha, a2=a2)
        # Second option: when a query is given, create from database
        elif query is not None:
            self.from_database(query, filename_database, query_option)
        elif molecule is not None and filename_gc_database is not None:
            self.from_group_contribution_method(molecule, filename_gc_database)
        else:
            raise Exception("Could not create permittivity parameters.")
        
        # Check if parameters are nan and set them 0 in this case
        if self.n_comp == 1:
            if np.isnan(self.a11_mu2):
                self.a11_mu2 = 0
            if np.isnan(self.a12_alpha):
                self.a12_alpha = 0
            if np.isnan(self.a2):
                self.a2 = 0

    def __str__(self):

        return "a11_mu2: " + str(self.a11_mu2) + ", a12_alpha: " + str(self.a12_alpha) + ", a2: " + str(self.a2)
    
    def from_parameter_values(self, a11_mu2, a12_alpha, a2):

        """ Create relative static permittivity parameters directly from parameter values.

        Parameters
        ----------
            a11_mu2: f64 or list(f64)
            Dipole scaling parameter.

            a12_alpha: f64 or list(f64)
            Polarizability scaling parameter

            a2: f64 or list(f64)
            Correlation integral parameter

        """

        if type(a11_mu2) is list:
            self.n_comp = len(a11_mu2)
        else:
            self.n_comp = 1

        if self.n_comp == 1:
            if a11_mu2 is not None:
                if np.isnan(a11_mu2):
                    self.a11_mu2 = 0
                else:
                    self.a11_mu2 = a11_mu2

            if a12_alpha is not None:
                if np.isnan(a12_alpha):
                    self.a12_alpha = 0
                else:
                    self.a12_alpha = a12_alpha

            if a2 is not None:
                if np.isnan(a2):
                    self.a2 = 0
                else:
                    self.a2 = a2

        # for a mixture
        else:

            self.a11_mu2 = []
            self.a12_alpha = []
            self.a2 = []

            for i in range(self.n_comp):
                if a11_mu2[i] is not None:
                    if np.isnan(a11_mu2[i]):
                        self.a11_mu2.append(0)
                    else:
                        self.a11_mu2.append(a11_mu2[i])
                else:
                    self.a11_mu2.append(0)

                if a12_alpha[i] is not None:
                    if np.isnan(a12_alpha[i]):
                        self.a12_alpha.append(0)
                    else:
                        self.a12_alpha.append(a12_alpha[i])
                else:
                    self.a12_alpha.append(0)

                if a2[i] is not None:
                    if np.isnan(a2[i]):
                        self.a2.append(0)
                    else:
                        self.a2.append(a2[i])
                else:
                    self.a2.append(0)


 

    def from_database(self, query, filename_database, query_option):

        """ Create relative static permittivity parameters from database.

        Parameters
        ----------
            query: List 
            List with queries

            filename_database: str
            Filename of database

            query_option: str
            Search option

        """

        # Load database
        database = pd.read_csv(filename_database, index_col=query_option)

        if type(query) is list:
            self.n_comp = len(query)
        else:
            self.n_comp = 1

        if self.n_comp == 1:
            if query in database.index:
                self.a11_mu2 = database.loc[(query, "a11_mu2")]
                self.a12_alpha = database.loc[(query, "a12_alpha")]
                self.a2 = database.loc[(query, "a2")]
                self.status = 0
            else:
                self.status = -1
        
        else:

            # Prepare parameters
            self.a11_mu2 = []
            self.a2 = []
            self.a12_alpha = []

            # Check if queries are in database
            for q in query:
                if q in database.index:
                    self.a11_mu2.append(database.loc[(q, "a11_mu2")])
                    self.a12_alpha.append(database.loc[(q, "a12_alpha")])
                    self.a2.append(database.loc[(q, "a2")])
                    self.status = 0
                else:
                    self.status = -1
                    break


    def from_group_contribution_method(self, molecule, filename_gc_database):

        """ Create relative static permittivity parameters from group contribution method.

        Parameters
        ----------
            molecule: dict or list(dict)
            Dictionary with groups as keys and frequency as values.

            filename_gc_database: str
            Filename of GC database, default: database from publication.

        """

        # Read GC parameter database
        gc_parameters = pd.read_csv(filename_gc_database, index_col="group")
        all_groups = gc_parameters.index.to_list()

        if type(molecule) is list:
            self.n_comp = len(molecule)
        else:
            self.n_comp = 1

        if self.n_comp == 1:
            # check if all groups in molecule occur in gc_parameters
            for g in molecule.keys():
                if g not in all_groups:
                    raise Exception("Group " + g + " not in GC database.")
                
            self.a11_mu2 = np.sum([molecule[g] * gc_parameters.loc[g, "a11_mu2"] for g in molecule.keys()])
            self.a2 = np.sum([molecule[g] * gc_parameters.loc[g, "a2"] for g in molecule.keys()])
            self.a12_alpha = np.sum([molecule[g] * gc_parameters.loc[g, "a12_alpha"] for g in molecule.keys()])

        else:
            self.a11_mu2 = []
            self.a12_alpha = []
            self.a2 = []

            for m in molecule:
                # check if all groups in molecule occur in gc_parameters
                for g in m.keys():
                    if g not in all_groups:
                        raise Exception("Group " + g + " not in GC database.")
                
                self.a11_mu2.append(np.sum([m[g] * gc_parameters.loc[g, "a11_mu2"] for g in m.keys()]))
                self.a2.append(np.sum([m[g] * gc_parameters.loc[g, "a2"] for g in m.keys()]))
                self.a12_alpha.append(np.sum([m[g] * gc_parameters.loc[g, "a12_alpha"] for g in m.keys()]))
            

        
