import numpy as np
import pandas as pd
import Molecule.fragmentation_algorithm as fragmentation_algorithm
from feos.si import *

class Fragmentation:

    def __init__(self, fragmentation_path="Molecule/Fragmentation.csv"):

        """ Initialize fragmentation class.

        Parameters
        ----------
        fragmentation_path: str, optional
            Directory to csv-file with fragmentation to pre-load fragmentation.

        """

        # Load existing fragmentation: either csv or xlsx file
        if fragmentation_path is not None:
            self.df = pd.read_csv(fragmentation_path, index_col="substance_id")
            # set all groups
            self.all_groups = list(self.df.columns)
            # filter columns so that only groups are there
            for e in ['n_functional_groups', 'smiles', 'common_name', 'iupac_name', 'cas', 'success', 'family']:
                if e in list(self.df.columns):
                    self.all_groups.remove(e)
            # separate groups into polar and nonpolar groups
            self.components = self.df.index.unique()
            self.nonpolar = []
            self.polar = []
            for seg in self.all_groups:
                if "O" in seg or "N" in seg or seg == "CtCH":
                    self.polar.append(seg)
                else:
                    self.nonpolar.append(seg)
        else:
            self.df = pd.DataFrame()

    
    def get_fragmentation(self, substance=None, smiles=None):
        
        """ Reads dataframe with fragmentation information 
        and returns info about fragmentation for a given substance.

        Parameters
        ----------
        substance: int
            substance_id of substance for which fragmentation is desired.

        smiles: str
            smiles code of substance for which fragmentation is desired.

        Returns
        ----------
        molecule: dict
            Dictionary with groups as keys and frequency as values.

        success: 0 or 1
            Indicates if successful fragmentation exists for this molecule.

        n_functional_groups: int
            Number of functional groups (polar/associating groups).

        """
        if substance is None and smiles is not None:
            substance = smiles
            
        # Get fragmentation from dataframe
        try:
            fragmentation_line = self.df.loc[[substance]]
            success = fragmentation_line.success.iloc[0]
            n_functional_groups = fragmentation_line.n_functional_groups.iloc[0]
        except:
            success = 0
            n_functional_groups = 0
        # put into dictionary "molecule"
        if success:
            molecule = {i: fragmentation_line[i].iloc[0]
                            for i in self.all_groups if fragmentation_line[i].iloc[0] > 0}
        else:
            molecule = {}
        
        return molecule, success, n_functional_groups

    
    def new_fragmentation(self, smiles):

        """ Gets the fragmentation for a given smiles.

        Parameters
        ----------
        smiles: llist(str
            List with smiles code of the molecules to fragment.

        Returns
        ----------
        df_fragmentation: pandas.DataFrame
            Dataframe with fragmentation information for all molecules.

        """

        # get info about groups
        smarts = fragmentation_algorithm.get_smarts(sauer=1)    
        all_groups = pd.read_csv("Parameters/GCPermittivityParameters.csv", index_col="group").index.to_list()
        functional_groups = fragmentation_algorithm.get_functional_groups()
        fragments_sauer = fragmentation_algorithm.get_fragments_sauer()

        # Set up empty fragmentation DataFrame
        df_fragmentation = pd.DataFrame(index=smiles)
        
        df_fragmentation["success"] = np.zeros(len(smiles))
        df_fragmentation["n_functional_groups"] = np.zeros(len(smiles))
        for group in all_groups:
            df_fragmentation[group] = np.empty(len(smiles)) * np.nan

        # Carry out fragmentation             
        for smi in smiles:
   
            try:
                
                # Fragment molecules and get fragmenation dictionary
                groups, _ = fragmentation_algorithm.fragment_to_sauer_groups(smi)
                molecule = self.get_molecule_from_group_list(groups)
                
                # Check if all groups are allowed as groups
                for g, n in zip(molecule.keys(), molecule.values()):
                    if g not in all_groups:
                        raise Exception("Not fragmentable with given groups :(")

                # Save successful fragmentation in DataFrame 
                for g, n in zip(molecule.keys(), molecule.values()):
                    df_fragmentation.loc[(smi, g)] = n

                # Count functional groups
                sum_functional = 0
                for g, n in zip(molecule.keys(), molecule.values()):
                    if g in functional_groups:
                        sum_functional += n
                df_fragmentation.loc[(smi, "n_functional_groups")] = sum_functional
            
                # Set success to 1
                df_fragmentation.loc[(smi, "success")] = 1

            except:

                pass
        
        # Update self
        self.df = df_fragmentation

        return df_fragmentation
    
    def get_molecule_from_group_list(self, groups):

        """ Returns a dictionary with the fragmentation from a list of groups.

        Parameters
        ----------
        groups: list(str)
            Fragmentation of a molecule as a list of groups: 
            e.g., ["CH3", "CH2", "CH2", "CH3"] for butane.

        Returns
        ----------
        molecule: dict
            Dictionary with groups as keys and frequency as values.

        """

        molecule = {}

        for g in groups:
            if g not in molecule.keys():
                molecule[g] = 1
            else:
                molecule[g] += 1

        return molecule
